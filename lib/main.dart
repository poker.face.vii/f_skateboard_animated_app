import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:skateboard_animate/onboarding.dart';
import 'package:skateboard_animate/provider/indexNotifire.dart';

void main() => runApp(SkateboardApp());

class SkateboardApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(debugShowCheckedModeBanner: false,
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: ChangeNotifierProvider(
          create: (context) => IndexNotifier(),
          child: OnBoarding(),
        ));
  }
}
