import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:skateboard_animate/provider/indexNotifire.dart';

class OnBoarding extends StatefulWidget {
  @override
  _OnBoardingState createState() => _OnBoardingState();
}

class _OnBoardingState extends State<OnBoarding> {
  PageController _pageController;
  @override
  void initState() {
    SystemChrome.setEnabledSystemUIOverlays([]);
    _pageController = PageController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0XFFF2F2F2),
      body: Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.fromLTRB(20, 16, 20, 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  'Hops',
                  style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
                ),
                Stack(
                  children: <Widget>[
                    IconButton(
                      icon: Icon(Icons.shopping_basket),
                      onPressed: () {},
                    ),
                    Positioned(
                        right: 10,
                        bottom: 8,
                        child: Container(
                          width: 12,
                          height: 12,
                          decoration: BoxDecoration(
                              color: Colors.white, shape: BoxShape.circle),
                        ))
                  ],
                )
              ],
            ),
          ),
          Expanded(
            flex: 1,
            child: PageView(
              controller: _pageController,
              onPageChanged: (int index) {
                Provider.of<IndexNotifier>(context).index = index;
              },
              children: <Widget>[
                Text('data'),
                Text('data 2'),
              ],
            ),
          )
        ],
      ),
    );

    
  }
}
